<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%post}}`.
 */
class m231016_075813_add_column_to_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'status', $this->boolean()->defaultValue('0'));
    }

    public function down()
    {
        $this->dropColumn('user', '8');
    }
}
