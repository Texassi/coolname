<?php

namespace app\controllers;

use yii\web\Controller;

class MyController extends Controller
{
    public function actionIndex($fio = 'Укажите фио')
    {

        return $fio;
    }

    public function actionAbout()
    {
        return $this->render('about' ,[
        'lol' => 'ещкерее'

        ]);

    }

    public function actionKek()
    {
        return $this->render('kek' ,[
            'wow' => 'bkkk'

        ]);

    }

    public function actionWtf()
    {
        return $this->render('wtf', [
            'privet' => 'kak dela'

        ]);


    }

    public function actionMail()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->render();
        }


        return $this->render('mail', [
            'modal'=>$model
        ]);

    }

}

