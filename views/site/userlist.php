<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<h1>Userlist</h1>
<table>
    <?php foreach ($userlist as $i => $model): ?>
        <tr>
            <td><?=$model->id;?></td>
            <td><?=$model->username;?></td>
            <td><?=$model->status;?></td>
            <td>
                <?php
                    if ($userlist[$i]->status == '0'):
                    {
                        echo '<a class="btn btn-danger" href="' . Url::to(["/site/up-status", "id" => $model->id]) . '">' . "Deactivate" . '</a>';
                        }
                    else:
                    {
                        echo '<a class="btn btn-primary" href="' . Url::to(["/site/up-status", "id" => $model->id]) . '">' . "Activate" . '</a>';
                        }
                    endif;
                ?>
            </td>
        </tr>
    <?php endforeach;?>
</table>